#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf usr mplayer-vaapi-20100602
mkdir -p usr
tar -jxf mplayer-vaapi-20100602-FULL.tar.bz2
cd mplayer-vaapi-20100602/mplayer-vaapi
patch -p1 < ../patches/mplayer-vaapi.patch
patch -p1 < ../patches/mplayer-vaapi-gma500-workaround.patch
patch -p1 < ../patches/mplayer-vaapi-0.29.patch
patch -p1 < ../patches/mplayer-vdpau.patch
#./configure --enable-xv --enable-xvmc --disable-vaapi --disable-vdpau --disable-ivtv --prefix=$p/usr
./configure --cc="$CC" --host-cc="$CC" --extra-cflags="$CFLAGS" --enable-xv --enable-xvmc --disable-vaapi --disable-vdpau --disable-ivtv --prefix=$p/usr
make -j $procs
make install
#Max. number of threads can be 8 for mencoder
mprocs=$procs
if (($mprocs > 8))
then mprocs=8
fi
before=`date +%s.%N`
$p/usr/bin/mencoder $p/../extra/pts-trondheim.avi -o /dev/null -ovc lavc -oac copy -lavcopts vcodec=mpeg4:threads=$mprocs:mbd=2:trell=1:v4mv=1:vstrict=1
after=`date +%s.%N`
echo "mencoder run time: `echo "$after - $before" | bc`" > $p/log
cd $p; rm -rf usr mplayer-vaapi-20100602
