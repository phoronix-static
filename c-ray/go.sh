#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf c-ray-1.1
tar -zxf c-ray-1.1.tar.gz
cd c-ray-1.1/
make -j $procs CFLAGS="$CFLAGS"
before=`date +%s.%N`
./c-ray-mt -t $(($procs * 16)) -s 1600x1200 -r 8 -i sphfract -o output.ppm > $p/log 2>&1
after=`date +%s.%N`
echo "c-ray run time: `echo "$after - $before" | bc`" >> $p/log
cd $p; rm -rf c-ray-1.1
