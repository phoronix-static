#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf usr
mkdir -p usr
cd usr
tar -zxf $p/Fhourstones.tar.gz
make -j $procs CFLAGS="$CFLAGS" CC="$CC"
before=`date +%s.%N`
./SearchGame < inputs > $p/log 2>&1
after=`date +%s.%N`
echo "fhourstones run time: `echo "$after - $before" | bc`" >> $p/log
cd $p; rm -rf usr
