#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf systester-1.1.0-src
tar -jxf systester-1.1.0-src.tar.bz2
cd systester-1.1.0-src
sed -i -e "s/cc/$CC/g" cli/Makefile
make -j $procs -C $p/systester-1.1.0-src/cli CFLAGS="-O2 -pipe -Wall -W -D_REENTRANT -DQT_NO_DEBUG -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED $CFLAGS" CXXFLAGS="-O2 -pipe -Wall -W -D_REENTRANT -DQT_NO_DEBUG -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED $CXXFLAGS"
before=`date +%s.%N`
for arg in {"-gausslg","-qcborwein"}; do
    $p/systester-1.1.0-src/cli/systester-cli -bench $arg 4M -threads $procs > $p/log 2>&1
done
after=`date +%s.%N`
echo "systester run time: `echo "$after - $before" | bc`" >> $p/log
cd $p; rm -rf systester-1.1.0-src
