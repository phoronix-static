#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf ramsmp-3.5.0
tar -zxf ramsmp-3.5.0.tar.gz
cd ramsmp-3.5.0/
sed -i -e "s/read ANS//g" build.sh
sed -i -e "s/-Wall -O/$CFLAGS -Wall -O/g" build.sh
./build.sh
>$p/log
before=`date +%s.%N`
for arg in {"COPY","SCALE","ADD","TRIAD","AVERAGE","-b 3","-b 6"}; do
    ./ramsmp $arg -l 10 >> $p/log 2>&1
done
after=`date +%s.%N`
echo "ramspeed run time: `echo "$after - $before" | bc`" >> $p/log
sed -i -e'/^$/d' -e'/RAMspeed/d' -e'/USAGE/d' -e'/specifi/d' -e'/INTmark/d' -e'/INTmem/d' -e'/enables/d' -e'/displays/d' -e'/---/d' $p/log
cd $p; rm -rf ramsmp-3.5.0
