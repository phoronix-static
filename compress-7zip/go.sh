#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf p7zip_9.13
tar -jxf p7zip_9.13_src_all.tar.bz2
cd p7zip_9.13
make -j $procs OPTFLAGS="$CXXFLAGS"
before=`date +%s.%N`
./bin/7za b > $p/log 2>&1
after=`date +%s.%N`
echo "compress-7zip run time: `echo "$after - $before" | bc`" >> $p/log
cd $p; rm -rf p7zip_9.13
