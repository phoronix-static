#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf dcraw.c dcraw-bin *.NEF *.ppm
tar -jxf dcraw-test-1.tar.bz2
gcc -o dcraw-bin $CFLAGS dcraw.c -lm -DNO_JPEG -DNO_LCMS
before=`date +%s.%N`
./dcraw-bin -q 3 -4 -f -a *.NEF 2>&1
after=`date +%s.%N`
echo "dcraw run time: `echo "$after - $before" | bc`" > $p/log
rm -rf dcraw.c dcraw-bin *.NEF *.ppm
