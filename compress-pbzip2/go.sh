#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf bzip2-1.0.5 pbzip2-1.0.5
tar -zxf bzip2-1.0.5.tar.gz
tar -zxf pbzip2-1.0.5.tar.gz
cd bzip2-1.0.5
make -j $procs CFLAGS="$CFLAGS"
cp -f libbz2.a ../pbzip2-1.0.5
cp -f bzlib.h ../pbzip2-1.0.5
cd ..
cd pbzip2-1.0.5/
make pbzip2-static
echo "Copy 256M from /dev/urandom." > $p/log                                                                                   
dd if=/dev/urandom of=file bs=1M count=256 >> $p/log 2>&1
before=`date +%s.%N`
./pbzip2 -c -p$procs -r -5 ./file > /dev/null 2>&1
after=`date +%s.%N`
echo "compress-pbzip2 run time: `echo "$after - $before" | bc`" >> $p/log
cd $p; rm -rf bzip2-1.0.5 pbzip2-1.0.5
