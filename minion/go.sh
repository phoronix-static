#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf minion-0.9
tar -zxf minion-0.9-src.tar.gz
cd minion-0.9
mkdir build
cd build
CPU=""
cmake -DQUICK=1 ..
make clean
make minion -j $procs CXX_FLAGS="$CXXFLAGS" CXX="$CXX"
> $p/log
before=`date +%s.%N`
for arg in {"benchmarks/Bibd/bibdline11.minion","benchmarks/graceful/k6p2_table.minion","benchmarks/Quasigroup/qg-watchelement-7-10.minion","benchmarks/solitaire/solitaire_benchmark_8.minion"}; do
    ./minion-quick $p/minion-0.9/$arg >> $p/log 2>&1
done
after=`date +%s.%N`
echo "minion run time: `echo "$after - $before" | bc`" >> $p/log
sed -i -e '/Sol:/d' -e '/^$/d' $p/log
cd $p; rm -rf minion-0.9
