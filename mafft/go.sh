#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf usr mafft-6.706-without-extensions
mkdir -p usr
tar -zxf mafft-6.706-without-extensions-src.tgz
cd mafft-6.706-without-extensions/core
make clean
sed -i -e "s|PREFIX = /usr/local|PREFIX = $p/usr|g" Makefile
make -j $procs CFLAGS="$CFLAGS"
make install
cp -f $p/mafft-6.706-without-extensions/scripts/mafft $p/usr
bunzip2 $p/pyruvate_decarboxylase.fasta.bz2 -c > $p/usr/pyruvate_decarboxylase.fasta
cd $p/usr
before=`date +%s.%N`
./mafft --localpair --maxiterate 10000 pyruvate_decarboxylase.fasta 1>/dev/null 2>&1
after=`date +%s.%N`
echo "mafft run time: `echo "$after - $before" | bc`" > $p/log
cd $p; rm -rf usr mafft-6.706-without-extensions
