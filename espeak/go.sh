#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf usr gutenberg-science.txt espeak-1.40.02-source
mkdir -p usr
tar -zxf gutenberg-science.tar.gz
unzip -o espeak-1.40.02-source.zip
cd espeak-1.40.02-source/src/
sed -i -e "s|/usr|$p/usr|g" Makefile
sed -i -e "s|/usr|$p/usr|g" speech.h
make -j $procs CXXFLAGS="$CXXFLAGS"
make install
cd $p/usr/bin/
before=`date +%s.%N`
LD_LIBRARY_PATH=$p/usr/lib/:$LD_LIBRARY_PATH $p/usr/bin/espeak -f $p/gutenberg-science.txt -w /dev/null 2>&1
after=`date +%s.%N`
echo "espeak run time: `echo "$after - $before" | bc`" > $p/log
cd $p; rm -rf usr gutenberg-science.txt espeak-1.40.02-source
