#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

mkdir -p extra
cd extra

if ! wget http://www.phoronix-test-suite.com/benchmark-files/pts-sample-photos-2.tar.bz2; then
    wget http://www.phoronix.net/downloads/phoronix-test-suite/benchmark-files/pts-sample-photos-2.tar.bz2
fi
tar jxf pts-sample-photos-2.tar.bz2

if ! wget http://www.phoronix-test-suite.com/benchmark-files/pts-trondheim-avi.tar.bz2; then
    wget http://www.phoronix.net/downloads/phoronix-test-suite/benchmark-files/pts-trondheim-avi.tar.bz2
fi
tar jxf pts-trondheim-avi.tar.bz2

if ! wget http://www.phoronix-test-suite.com/benchmark-files/pts-trondheim-wav-3.tar.gz; then
    wget http://www.phoronix.net/downloads/phoronix-test-suite/benchmark-files/pts-trondheim-wav-3.tar.gz
fi
tar zxf pts-trondheim-wav-3.tar.gz

wget http://media.xiph.org/video/derf/y4m/soccer_4cif.y4m

