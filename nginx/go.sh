#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf usr httpd-2.2.11 nginx-0.8.53
mkdir -p usr
tar -zxf apache-ab-test-files-1.tar.gz
tar -zxf nginx-0.8.53.tar.gz
tar -zxf httpd-2.2.11.tar.gz
cd httpd-2.2.11/
./configure --prefix=$p/usr --enable-static-ab --without-http-cache
cd srclib/apr
make -j $procs CFLAGS="$CFLAGS"
cd ../apr-util
make -j $procs CFLAGS="$CFLAGS"
cd ../pcre
make -j $procs CFLAGS="$CFLAGS"
cd ../../support
make ab -j $procs CFLAGS="$CFLAGS"
cd ../..
cp -av httpd-2.2.11/support/ab $p/usr
cd nginx-0.8.53/
./configure --prefix=$p/usr --without-http_rewrite_module --without-http-cache
make -j $procs CFLAGS="$CFLAGS"
make install
cd ..

patch -p0 <<EOF
--- usr/conf/nginx.conf.orig  2010-11-09 18:22:34.000000000 +0200
+++ usr/conf/nginx.conf       2010-11-09 18:17:14.000000000 +0200
@@ -33,7 +33,7 @@
     #gzip  on;

     server {
+        listen       8088;
-        listen       80;
         server_name  localhost;

         #charset koi8-r;
EOF
mv -f test.html $p/usr/html/
mv -f pts.png $p/usr/html/
$p/usr/sbin/nginx
sleep 5
before=`date +%s.%N`
$p/usr/ab -n 500000 -c 100 http://localhost:8088/test.html > $p/log 2>&1
after=`date +%s.%N`
echo "nginx run time: `echo "$after - $before" | bc`" >> $p/log
$p/usr/sbin/nginx -s quit
sleep 3
cd $p; rm -rf usr httpd-2.2.11 nginx-0.8.53 CHANGE-APACHE-PORT.patch
