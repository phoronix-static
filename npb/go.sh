#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf NPB3.3
tar -zxf NPB3.3.tar.gz
cp $p/make.def $p/NPB3.3/NPB3.3-OMP/config/
cd NPB3.3/NPB3.3-OMP/
L="bt.A cg.B ep.B ft.B is.C lu.A mg.B sp.A ua.A"
for i in $L; do
    make ${i%.*} CLASS=${i#*.} FFLAGS="$FFLAGS"
done
export OMP_NUM_THREADS=$procs
> $p/log
before=`date +%s.%N`
for i in $L; do
    ./bin/$i >> $p/log 2>&1
done
after=`date +%s.%N`
echo "npb run time: `echo "$after - $before" | bc`" >> $p/log
sed -i -e'/E+/d' -e'/E-/d' -e'/D+/d' -e'/Step/d' -e'/step/d' $p/log
cd $p; rm -rf NPB3.3
