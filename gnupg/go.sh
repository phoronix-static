#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf usr gnupg-1.4.10
mkdir -p usr
tar -zxvf gnupg-1.4.10.tar.gz
cd gnupg-1.4.10/
./configure --prefix=$p/usr
make -j $procs CFLAGS="$CFLAGS"
make install
echo pts-1234567890 > passphrase
dd if=/dev/zero of=encryptfile bs=1M count=1024
before=`date +%s.%N`
$p/usr/bin/gpg -c --no-options --passphrase-file passphrase -o /dev/null encryptfile 2>&1
after=`date +%s.%N`
echo "gnupg run time: `echo "$after - $before" | bc`" > $p/log
cd $p; rm -rf usr gnupg-1.4.10
