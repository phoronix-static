#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf httpd-2.2.17 usr
tar -zxf httpd-2.2.17.tar.gz
mkdir -p usr
cd httpd-2.2.17
./configure --prefix=$p/usr
make -j $procs CFLAGS="$CFLAGS -DHAVE_EXPAT_CONFIG_H"
make install
cd $p
cp test.html usr/htdocs/
cp pts.png usr/htdocs/
patch -p0 <<EOF
--- usr/conf/httpd.conf.orig	2009-05-05 11:45:32.000000000 -0400
+++ usr/conf/httpd.conf	2009-05-05 11:46:09.000000000 -0400
@@ -37,7 +37,7 @@
 # prevent Apache from glomming onto all bound IP addresses.
 #
 #Listen 12.34.56.78:80
-Listen 80
+Listen 8088
 
 #
 # Dynamic Shared Object (DSO) Support
EOF
$p/usr/bin/apachectl -k start -f $p/usr/conf/httpd.conf
sleep 5
before=`date +%s.%N`
$p/usr/bin/ab -n 700000 -c 100 http://localhost:8088/test.html > $p/log 2>&1
after=`date +%s.%N`
echo "apache run time: `echo "$after - $before" | bc`" >> $p/log
$p/usr/bin/apachectl -k stop
cd $p; rm -rf httpd-2.2.17 usr
