#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf usr libgpg-error-1.7 libgcrypt-1.4.4
mkdir -p usr
tar -jxf libgpg-error-1.7.tar.bz2
tar -jxf libgcrypt-1.4.4.tar.bz2
cd libgpg-error-1.7
./configure --prefix=$p/usr
make -j $procs CFLAGS="$CFLAGS"
make install
cd ../libgcrypt-1.4.4
./configure --with-gpg-error-prefix=$p/usr
make -j $procs CFLAGS="$CFLAGS"
before=`date +%s.%N`
$p/libgcrypt-1.4.4/tests/benchmark --cipher-repetition 100 cipher camellia256 > $p/log 2>&1
after=`date +%s.%N`
echo "gcrypt run time: `echo "$after - $before" | bc`" >> $p/log
cd $p; rm -rf usr libgpg-error-1.7 libgcrypt-1.4.4

