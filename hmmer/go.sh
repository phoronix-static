#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf usr hmmer-2.3.2
mkdir -p usr
tar -zxvf hmmer-2.3.2.tar.gz
cd hmmer-2.3.2
./configure --enable-threads --prefix=$p/usr
make -j $procs CFLAGS="$CFLAGS"
make install
cp -r $p/hmmer-2.3.2/tutorial $p/usr
gunzip $p/Pfam_ls.gz -c > $p/usr/tutorial/Pfam_ls
cd $p/usr/tutorial
before=`date +%s.%N`
../bin/hmmpfam -E 0.1 Pfam_ls 7LES_DROME > /dev/null
after=`date +%s.%N`
echo "hmmer run time: `echo "$after - $before" | bc`" > $p/log
cd $p; rm -rf usr hmmer-2.3.2

