#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf usr gmp-4.3.0 gmpbench-0.1 gexpr.c
mkdir -p usr
tar -zxvf gexpr.c.tar.gz
tar -zxvf gmp-4.3.0.tar.gz
tar -zxvf gmpbench-0.1.tar.gz
cp gexpr.c gmpbench-0.1
cd gmp-4.3.0/
./configure --prefix=$p/usr
make -j $procs CFLAGS="$CFLAGS"
make install
cp $p/usr/include/gmp.h $p/gmpbench-0.1/
cd ../gmpbench-0.1/
gcc -lm $CFLAGS gexpr.c -o gexpr
before=`date +%s.%N`
LIBS=$p/usr/lib/libgmp.so.3.5.0 PATH=.:$PATH ./runbench > $p/log 2>&1
after=`date +%s.%N`
echo "gmpbench run time: `echo "$after - $before" | bc`" >> $p/log
cd $p; rm -rf usr gmp-4.3.0 gmpbench-0.1 gexpr.c

