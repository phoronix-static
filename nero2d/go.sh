#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf usr nero2d-2.0.2
mkdir -p usr
tar -zxf nero2d-2.0.2.tar.gz

patch -p0 <<'EOT'
--- nero2d-2.0.2.orig/src/nexus/nexus.cpp	2009-04-03 09:42:29.000000000 -0400
+++ nero2d-2.0.2/src/nexus/nexus.cpp	2009-06-13 21:52:42.589304348 -0400
@@ -21,6 +21,8 @@
 #include "nexus.h"
 #include "engine.h"
 #include "base.h"
+#include <cstring>
+#include <cstdio>
 
 using namespace std;
 
EOT

cd nero2d-2.0.2/
./configure --prefix=$p/usr
make -j $procs CXXFLAGS="$CXXFLAGS" CFLAGS="$CFLAGS"
make install
before=`date +%s.%N`
$p/usr/bin/nero2d $p/usr/share/examples/example2/example2.igf > /dev/null 2>&1
after=`date +%s.%N`
echo "nero2d run time: `echo "$after - $before" | bc`" > $p/log
cd $p; rm -rf usr nero2d-2.0.2
