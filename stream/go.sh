#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -f stream-bin
gcc $CFLAGS stream.c -O2 -fopenmp -o stream-bin -DNTIMES=5000
export OMP_NUM_THREADS=$procs
before=`date +%s.%N`
./stream-bin > $p/log 2>&1
after=`date +%s.%N`
echo "stream run time: `echo "$after - $before" | bc`" >> $p/log
rm -f stream-bin