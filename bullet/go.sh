#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf bullet-2.75
tar -zxf bullet-2.75.tgz
cd bullet-2.75
cmake .
make AppBenchmarks -j $procs CFLAGS="$CFLAGS" CXXFLAGS="$CXXFLAGS"
cd Demos/Benchmarks
before=`date +%s.%N`
./AppBenchmarks > $p/log 2>&1
after=`date +%s.%N`
echo "" >> $p/log
echo "bullet run time: `echo "$after - $before" | bc`" >> $p/log
sed -i -e'/\.\./d' -e'/BenchmarkDemo:/d' -e'/rays in/d' -e'/Profil/d' -e'/--/d' -e'/Unaccounted:/d' $p/log
cd $p; rm -rf bullet-2.75
