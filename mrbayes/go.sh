#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf usr mpich2-1.0.8p1 mrbayes-3.1.2
mkdir -p usr
tar -zxf mpich2-1.0.8p1.tar.gz
tar -zxf mrbayes-3.1.2.tar.gz
cd $p/mpich2-1.0.8p1/
./configure --prefix=$p/usr --enable-fast=all --with-pm=gforker --disable-option-checking
if ! make -j $procs; then
    make
fi
make install
cd $p/mrbayes-3.1.2/
sed -i -e "s/MPI ?= no/MPI ?= yes/g" Makefile
sed -i -e "s/USEREADLINE ?= yes/USEREADLINE ?= no/g" Makefile
sed -i -e "s/define WIN_VERSION/define UNIX_VERSION/g" mb.h
make -j $procs PATH=$p/usr/bin/:$PATH CFLAGS="$CFLAGS" ARCHITECTURE=unix
cat>job.nex<<EOT
begin mrbayes;
   set autoclose=yes nowarn=yes;
   execute primates.nex;
   lset nst=6 rates=invgamma;
   mcmc ngen=30000 samplefreq=10;
   sump burnin=250;
   sumt burnin=250;
end;
EOT
before=`date +%s.%N`
$p/usr/bin/mpiexec -np $procs ./mb job.nex 1>/dev/null 2>&1
after=`date +%s.%N`
echo "mrbayes run time: `echo "$after - $before" | bc`" > $p/log
cd $p; rm -rf usr mpich2-1.0.8p1 mrbayes-3.1.2
