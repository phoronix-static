#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf usr libxml2-2.6.31 php-5.2.9 phpbench-0.8.1 
mkdir -p usr
tar -zxf libxml2-2.6.31.tar.gz
tar -jxf php-5.2.9.tar.bz2
tar -zxf phpbench-0.8.1.tar.gz
cd $p/libxml2-2.6.31
./configure --prefix=$p/usr
make -j $procs CFLAGS="$CFLAGS"
make install
cd $p/php-5.2.9
./configure --with-libxml-dir=$p/usr --prefix=$p/usr
make -j $procs CFLAGS="$CFLAGS"
make install
cd $p/phpbench-0.8.1
before=`date +%s.%N`
$p/usr/bin/php phpbench.php -i 100000 | sed -e "/^Warning.*$/d" -e "/^$/d" > $p/log 2> /dev/null
after=`date +%s.%N`
echo "phpbench run time: `echo "$after - $before" | bc`" >> $p/log
sed -i -e'/enabled/d' $p/log
cd $p; rm -rf usr libxml2-2.6.31 php-5.2.9 phpbench-0.8.1
