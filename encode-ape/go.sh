#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf $p/usr $p/mac-3.99-u4-b5-s6
mkdir -p usr
tar -zxf mac-3.99-u4-b5-s6.tar.gz
cd mac-3.99-u4-b5-s6/
CXXFLAGS="$CXXFLAGS -DSHNTOOL" ./configure --prefix=$p/usr
make -j $procs
make install
cd $p
before=`date +%s.%N`
$p/usr/bin/mac $p/../extra/pts-trondheim-3.wav /dev/null -c4000 > /dev/null 2>&1
after=`date +%s.%N`
echo "encode-ape run time: `echo "$after - $before" | bc`" > $p/log
rm -rf $p/usr $p/mac-3.99-u4-b5-s6
