#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf usr povray-3.6.1
tar -jxf povray-3.6.1.tar.bz2
mkdir -p usr
cd povray-3.6.1/
./configure --prefix=$p/usr COMPILED_BY="PhoronixTestSuite"
make -j $procs CFLAGS="$CFLAGS"
make install
before=`date +%s.%N`
echo 1 | $p/usr/bin/povray -benchmark > $p/log 2>&1
after=`date +%s.%N`
echo "povray run time: `echo "$after - $before" | bc`" >> $p/log
sed -i -e'/Building Photon/d' $p/log
cd $p; rm -rf usr povray-3.6.1
