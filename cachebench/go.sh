#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf llcbench
tar -zxf llcbench.tar.gz
cd llcbench
make linux-mpich
make cache-bench CB_CFLAGS="$CFLAGS"
cd cachebench
before=`date +%s.%N`
./cachebench -m 16 -e 1 -x 2 -d 5 > $p/log
after=`date +%s.%N`
echo "cachebench run time: `echo "$after - $before" | bc`" >> $p/log
cd $p; rm -rf llcbench
