#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf usr postgresql-9.0.1
mkdir -p usr
tar -xjf postgresql-9.0.1.tar.bz2 
sed -i -e 's/__FAST_MATH__/foo_bar/g' $p/postgresql-9.0.1/configure
sed -i -e 's/__FAST_MATH__/foo_bar/g' $p/postgresql-9.0.1/src/backend/utils/adt/date.c
sed -i -e 's/__FAST_MATH__/foo_bar/g' $p/postgresql-9.0.1/src/backend/utils/adt/timestamp.c
sed -i -e 's/__FAST_MATH__/foo_bar/g' $p/postgresql-9.0.1/src/interfaces/ecpg/pgtypeslib/interval.c
sed -i -e 's/__FAST_MATH__/foo_bar/g' $p/postgresql-9.0.1/src/interfaces/ecpg/pgtypeslib/timestamp.c
cd postgresql-9.0.1
./configure --prefix=$p/usr --datadir=$p/usr/data --without-readline --without-zlib
make -j $procs CFLAGS="$CFLAGS"
make -C contrib/pgbench all
make install
make -C contrib/pgbench install
$p/usr/bin/initdb -D $p/usr/data/db --encoding=SQL_ASCII --locale=C
export PGDATA=$p/usr/data/db/
export PGPORT=7777
$p/usr/bin/pg_ctl start -o '-c checkpoint_segments=8 -c autovacuum=false'
sleep 30
$p/usr/bin/createdb pgbench
$p/usr/bin/pgbench -i -s $procs pgbench
sleep 10
before=`date +%s.%N`
$p/usr/bin/pgbench -t 30000 -c `expr $procs / 2` pgbench > $p/log
after=`date +%s.%N`
echo "pgbench run time: `echo "$after - $before" | bc`" >> $p/log
$p/usr/bin/dropdb pgbench
$p/usr/bin/pg_ctl stop
cd $p; rm -rf usr postgresql-9.0.1
