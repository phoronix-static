#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf scimark2_files
unzip -o scimark2_1c.zip -d scimark2_files
cd scimark2_files/
g++ $CXXFLAGS -o scimark2 -O *.c
before=`date +%s.%N`
./scimark2 -large > $p/log 2>&1
after=`date +%s.%N`
echo "scimark2 run time: `echo "$after - $before" | bc`" >> $p/log
cd $p; rm -rf scimark2_files/