#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf usr bash-4.2 sudokut0.4
mkdir -p usr
tar -zxf bash-4.2.tar.gz
tar -jxf sudokut0.4-1.tar.bz2
cd $p/bash-4.2
./configure --prefix=$p/usr/
make -j $procs CFLAGS="$CFLAGS"
make install
cd $p/sudokut0.4/
sed -i -e "s|/bin/bash|$p/usr/bin/bash|g" $p/sudokut0.4/sudokut
before=`date +%s.%N`
./sudokut-100-runs.sh > /dev/null 2>&1
after=`date +%s.%N`
echo "sudokut run time: `echo "$after - $before" | bc`" > $p/log
cd $p; rm -rf usr bash-4.2 sudokut0.4
