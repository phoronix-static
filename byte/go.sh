#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf bm
tar -zxf byte-benchmark-1.tar.gz
cd bm/
make clean
make CFLAGS="$CFLAGS -DTIME"
before=`date +%s.%N`
./Run dhry2 > $p/log
./Run register >> $p/log
./Run int >> $p/log
./Run float >> $p/log
after=`date +%s.%N`                                                                                                            
echo "byte run time: `echo "$after - $before" | bc`" >> $p/log
sed -i -e '/#/d' -e '/^$/d' $p/log
cd $p; rm -rf bm
