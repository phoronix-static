#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf version1.0
tar -zxf qn24b-version1.0.tgz
cd version1.0/omp/
sed -i -e "s/-Wall -static -O2/-Wall -static -fopenmp $CFLAGS/g" Makefile
make -j $procs
before=`date +%s.%N`
OMP_NUM_THREADS=$procs ./qn24b_openmp 18 > $p/log 2>&1
after=`date +%s.%N`
echo "n-queens run time: `echo "$after - $before" | bc`" >> $p/log
cd $p; rm -rf version1.0
