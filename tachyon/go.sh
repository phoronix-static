#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf tachyon outfile.tga
tar -zxf tachyon-0.98.7.tar.gz
cd tachyon/unix/
make all ARCH="linux-thr" CC="$CC" CFLAGS="$CFLAGS -Wall -fomit-frame-pointer -DLinux -DTHR -D_REENTRANT" AR="ar" ARFLAGS="r" STRIP="strip" RANLIB="ranlib" LIBS="-L. -ltachyon -lm -lpthread"
cd ..
before=`date +%s.%N`
$p/tachyon/compile/linux-thr/tachyon $p/tachyon/scenes/teapot.dat -numthreads $procs -fullshade -shade_phong -trans_vmd -aasamples 32 -res 1080 1080 > /dev/null 2>&1
after=`date +%s.%N`
echo "tachyon run time: `echo "$after - $before" | bc`" > $p/log
cd $p; rm -rf tachyon outfile.tga
