#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf clomp_v1.0
tar -zxf clomp_v1.0.tar.gz
cd clomp_v1.0
gcc -fopenmp $CFLAGS clomp.c -o clomp_build -lm
export OMP_NUM_THREADS=$procs
export KMP_BLOCKTIME=10000
before=`date +%s.%N`
./clomp_build -1 -1 64 100 32 1 100 > $p/log 2>&1
after=`date +%s.%N`
echo "clomp run time: `echo "$after - $before" | bc`" >> $p/log
sed -i -e'/OMP/d' -e'/calc_deposit/d' -e'/--/d' -e'/Serial/d' $p/log
cd $p; rm -rf clomp_v1.0
