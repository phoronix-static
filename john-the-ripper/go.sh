#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf john-1.7.3.1
tar -zxf john-1.7.3.1.tar.gz
cd $p/john-1.7.3.1/src/
make linux-x86-64 CFLAGS="-c -Wall -fomit-frame-pointer $CFLAGS"
cd $p/john-1.7.3.1/run/
before=`date +%s.%N`
./john --test > $p/log 2>&1
after=`date +%s.%N`
echo "john-the-ripper run time: `echo "$after - $before" | bc`" >> $p/log
cd $p; rm -rf john-1.7.3.1
