#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf crafty-23.3
unzip -o crafty-23.3.zip
cd crafty-23.3
make crafty-make -j $procs CFLAGS="$CFLAGS" CXFLAGS="$CXXFLAGS" target=LINUX CC=gcc CXX=g++ LDFLAGS="$LDFLAGS -lstdc++ -lpthread" opt="-DTRACE -DINLINE64 -DCPUS=2"
before=`date +%s.%N`
./crafty bench quit > $p/log 2>&1
after=`date +%s.%N`
echo "crafty run time: `echo "$after - $before" | bc`" >> $p/log
cd $p; rm -rf crafty-23.3

