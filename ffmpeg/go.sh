#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf usr ffmpeg-0.11.1
mkdir -p usr
tar -jxf ffmpeg-0.11.1.tar.bz2
sed -i -e '/-fno-tree-vectorize/d' $p/ffmpeg-0.11.1/configure
cd ffmpeg-0.11.1
./configure --disable-zlib --disable-yasm --prefix=$p/usr
make -j $procs CFLAGS="$CFLAGS"
make install
before=`date +%s.%N`
$p/usr/bin/ffmpeg -i $p/../extra/pts-trondheim.avi -threads $procs -y -target ntsc-vcd /dev/null 2>&1
after=`date +%s.%N`
echo "ffmpeg run time: `echo "$after - $before" | bc`" > $p/log
cd $p; rm -rf usr ffmpeg-0.11.1
