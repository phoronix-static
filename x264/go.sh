#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf usr x264-snapshot-20101122-2245
mkdir -p usr
tar -xjf x264-snapshot-20101122-2245.tar.bz2
cd x264-snapshot-20101122-2245
sed -i -e 's/-O3 -ffast-math//g' configure
sed -i -e 's/CFLAGS -fno-tree-vectorize/CFLAGS/g' configure
#./configure --prefix=$p/usr
./configure --prefix=$p/usr --disable-asm
make -j $procs CFLAGS="$CFLAGS -I. -std=gnu99"
make install
before=`date +%s.%N`
$p/usr/bin/x264 -o /dev/null --threads $procs $p/../extra/soccer_4cif.y4m > $p/log 2>&1
after=`date +%s.%N`
echo "x264 run time: `echo "$after - $before" | bc`" >> $p/log
sed -i -e'/eta/d' $p/log
cd $p; rm -rf usr x264-snapshot-20101122-2245
