#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf usr libpng-1.2.39 GraphicsMagick-1.3.12
mkdir -p usr
tar -zxf libpng-1.2.39.tar.gz
tar -jxf GraphicsMagick-1.3.12.tar.bz2
cd libpng-1.2.39
./configure --prefix=$p/usr/
make -j $procs CFLAGS="$CFLAGS"
make install
cd ../GraphicsMagick-1.3.12
LDFLAGS="-L$p/usr/lib" CPPFLAGS="-I$p/usr/include" ./configure --without-perl --prefix=$p/usr --with-png=yes
make -j $procs CFLAGS="$CFLAGS"
make install
> $p/log
before=`date +%s.%N`
for arg in {"-colorspace HWB","-blur 0x1.0","-lat 10x10-5%","-resize 50%","-sharpen 0x1.0"}; do
    OMP_NUM_THREADS=$procs $p/usr/bin/gm benchmark -duration 60 convert $p/../extra/DSC_6782.png $arg null: >> $p/log 2>&1
done
after=`date +%s.%N`
echo "graphics-magick run time: `echo "$after - $before" | bc`" >> $p/log
cd $p; rm -rf usr libpng-1.2.39 GraphicsMagick-1.3.12
