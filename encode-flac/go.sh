#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

p=`pwd`
rm -rf usr flac-1.2.1
mkdir -p usr
tar -zxf flac-1.2.1.tar.gz
patch -p0 <<EOF
diff -Naur flac-1.2.1-orig/examples/cpp/encode/file/main.cpp flac-1.2.1/examples/cpp/encode/file/main.cpp
--- flac-1.2.1-orig/examples/cpp/encode/file/main.cpp	2007-09-13 09:58:03.000000000 -0600
+++ flac-1.2.1/examples/cpp/encode/file/main.cpp	2007-11-18 12:59:45.000000000 -0600
@@ -30,6 +30,7 @@
 
 #include <stdio.h>
 #include <stdlib.h>
+#include <cstring>
 #include "FLAC++/metadata.h"
 #include "FLAC++/encoder.h"
 
EOF
cd flac-1.2.1
./configure --prefix=$p/usr
make -j $procs CFLAGS="$CFLAGS" CXXFLAGS="$CXXFLAGS"
make install
before=`date +%s.%N`
$p/usr/bin/flac -s --best --totally-silent $p/../extra/pts-trondheim-3.wav -f -o /dev/null 2>&1
after=`date +%s.%N`
echo "encode-flac run time: `echo "$after - $before" | bc`" > $p/log
cd $p; rm -rf usr flac-1.2.1
